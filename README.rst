Wordpress Theme Template
====================

Template project for Wordpress theme development.

Dependencies
------------

This project requires the following tools

* NodeJS
* Composer
* Gulp
* Bower

Ensure each of these tools are installed globally in your environment before
installing.
