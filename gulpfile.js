'use strict';

var gulp = require('gulp');
var util = require('gulp-util');
var path = require('path');
var del = require('del');
var plumber = require('gulp-plumber');

try {
  var config = require('./gulp.config')();
}
catch (e) {
  var config = null;
  var err = new util.PluginError("gulpfile.js", 'This project does not have a gulp.config.js file. Some tasks may fail.');
  util.log(err.toString());
}

var imgGlob = path.join('**', '*.{svg,jpg,png,ico}');

function gulpSrc(path) {
  return gulp.src(path)
    .pipe(plumber());
}

gulp.task('clean-build-styles', function() {
  return del(['build/theme/**/*.css', '!build/theme/vendor/**/*.css']);
});

gulp.task('clean-build-js', function() {
  return del(['build/theme/**/*.js', '!build/theme/vendor/**/*.js']);
});

gulp.task('clean-build-php', function() {
  return del(['build/**/*.php']);
});

gulp.task('clean-build-vendor', function() {
  return del(['build/theme/vendor/**/*.*']);
});

gulp.task('clean-build-composer', function() {
  return del(['build/theme/composer/**/*.*']);
});

gulp.task('clean-build-images', function() {
  return del([path.join('build', imgGlob)]);
});

gulp.task('clean-deploy', function() {
  // we know we are removing files outside of our working directory.
  return del([config.localDeploymentDirectory], {
    force: true
  });
});

gulp.task('build-php', ['clean-build-php'], function() {
  return gulpSrc('src/**/*.php')
    .pipe(gulp.dest('build/'));
});

gulp.task('build-js', ['clean-build-js'], function() {
  var uglify = require('gulp-uglify');

  return gulpSrc('src/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('build/'));
});

gulp.task('build-vendor', ['clean-build-vendor'], function() {
  return gulpSrc('bower_components/**/*.*')
    .pipe(gulp.dest('build/theme/vendor'));
});

gulp.task('build-composer', ['clean-build-composer'], function() {
  return gulpSrc('vendor/**/*.*')
    .pipe(gulp.dest('build/theme/composer'));
});

gulp.task('build-images', ['clean-build-images'], function() {
  var imageMin = require('gulp-imagemin');

  return gulpSrc(path.join('src', imgGlob))
    .pipe(imageMin())
    .pipe(gulp.dest('build/'));
});

gulp.task('build-styles', ['clean-build-styles'], function() {
  var sass = require('gulp-sass');

  return gulpSrc('src/**/*.sass')
    .pipe(sass({
      includePaths: [ 'node_modules/' ]
    }).on('error', sass.logError))
    .pipe(gulp.dest('build/'));
});

gulp.task('build', [
  'build-styles',
  'build-php',
  'build-images',
  'build-js',
  'build-vendor',
  'build-composer'
]);

gulp.task('deploy', ['clean-deploy', 'build'], function() {
  return gulpSrc('build/theme/**/*.*')
    .pipe(gulp.dest(config.localDeploymentDirectory));
});
