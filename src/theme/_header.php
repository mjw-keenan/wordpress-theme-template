<?php
$siteName = get_bloginfo('name') . ' &raquo ' . get_bloginfo('description');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 

    <title><?php wp_title('&raquo;', true, 'right'); ?></title>
    <meta name="author" content="Martin Keenan">

    <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
  </head>

  <body>
    <div class="page-header">
      <div class="container">
        <div class="header">
          <div class="site-logo">
            <a href="<?php echo home_url('/'); ?>">
            <?php
              // For SEO we do the following. If we are on the home page, we use
              // the site name as a H1 to indicate the most important part of the
              // page. Otherwise we use a DIV, with the expectation that the
              // page/article header will use a H1 and claim "most important".
            ?>
            <?php if (is_home() or is_front_page()): ?>
              <h1 class="header-title-text"><?php echo $siteName; ?></h1>
            <?php else: ?>
              <div class="header-title-text"><?php echo $siteName; ?></div>
            <?php endif; ?>
              <img src="<?php echo MJWKTemplates::$theme_uri; ?>/images/site-logo.png" alt="<?php echo $siteName; ?>" class="img-responsive" title="<?php echo $siteName; ?>" />
            </a>
          </div>
          <div class="col-xs-12 hidden-desktop">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="collapse navbar-collapse">
            <nav class="navbar navbar-default">
              <?php wp_nav_menu(array(
                'theme_location' => MJWKMenus::$header_menu,
                'container' => false,
                'menu_class' => MJWKMenus::$header_menu
              )); ?>
            </nav>
          </div>
        </div>
      </div>
    </div>
