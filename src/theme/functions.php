<?php

// include the library files, any module may depend on.
include('lib/templates.php');
include('lib/scripts.php');
include('lib/dependencies.php');

function mjwk_enqueue_assets()
{
    // enqueue the main style sheet for the project.
    wp_enqueue_style('style',
        MJWKTemplates::$theme_uri . '/style.css',
        array(),
        null
    );

    // add the bootstrap JS to the page too.
    wp_enqueue_script('bootstrap',
        MJWKTemplates::$theme_uri . '/vendor/bootstrap/dist/js/bootstrap.js',
        array('jquery'),
        null,
        true
    );

    // enable lazy loading of images
    wp_enqueue_script('jquery-lazyload',
        MJWKTemplates::$theme_uri . '/vendor/jquery_lazyload/jquery.lazyload.js',
        array('jquery'),
        null,
        true
    );
    wp_enqueue_script('lazyload',
        MJWKTemplates::$theme_uri . '/js/lazy-load.js',
        array('jquery-lazyload'),
        null,
        true
    );

}
add_action('wp_enqueue_scripts', 'mjwk_enqueue_assets');

include('includes/custom-posts.php');
include('includes/menus.php');
include('includes/acf.php');
include('includes/shortcodes.php');
