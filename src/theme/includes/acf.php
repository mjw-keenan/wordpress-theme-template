<?php
if(! function_exists("register_field_group"))
{
    // the plugin is not already included, we should include it.

    // Add the advanced custom field logic within the theme. We add ACF as a
    // composer dependency
    $custom_field_directory = MJWKTemplates::$theme_path . '/composer/advanced-custom-fields/';

    add_filter('acf/settings/path', 'mjwk_acf_settings_path');
    add_filter('acf/settings/dir', 'mjwk_acf_settings_path');
    function mjwk_acf_settings_path($path)
    {
        return $custom_field_directory;
    }

    include_once($custom_field_directory . 'acf.php');
}

if(function_exists("register_field_group"))
{
    // load our custom fields here.

}

