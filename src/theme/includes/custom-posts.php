<?php

class MJWKCustomPost
{

    public static function init()
    {
        $custom_posts = new MJWKCustomPost();
        $custom_posts->register_post_type();
    }

    public function register_post_type()
    {
        // register our custom-post types here. The basic format of our call:
        // register_post_type('custom-post-type',
        //     array(
        //         'labels' => array(
        //             'name' => 'Custom Posts',
        //             'singular_name' => 'Custom Post'
        //         ),
        //         'public' => true,
        //         'has_archive' => false,
        //         'supports' => array(
        //             'title', 'editor'
        //         ),
        //         'rewrite' => array(
        //             'slug' => $this->_post_slug('custom-post-type', 'custom-post-type')
        //         )
        //     )
        // );

    }

    private function _post_slug($post_type, $default)
    {
        // determine the "slug" that the post type should use. This is based on
        // the page which uses the page template. If no page uses the page
        // template, then we use the default slug.

        // retrieve the page which uses the template.
        $args = array(
            "post_type" => "page",
            "meta_key" => "_wp_page_template",
            "meta_value" => "archive-" . $post_type . ".php"
        );
        $posts = get_posts($args);

        // if a page uses our template, then this page slug is our slug,
        // otherwise we use the default.
        if ($posts)
        {
            return $posts[0]->post_name;
        }
        else
        {
            return $default;
        }
    }
}

add_action('init', array('MJWKCustomPost', 'init'));
