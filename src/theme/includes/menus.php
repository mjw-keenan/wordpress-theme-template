<?php

class MJWKMenus
{

    public static $header_menu = "header-menu";

    public static function init()
    {
        $menus = new MJWKMenus();
        $menus->register_nav_menu();
    }

    public function register_nav_menu()
    {
        // register the theme menus here.
        // register_nav_menu(...);
        register_nav_menu(self::$header_menu, 'Header Menu');

    }

    public static function get_menu_items($menu_name)
    {
        // given a menu name, as it is registered in the register_nav_menu
        // function, returns a list of the menu items which are assigned to
        // that menu.
        $result = array();
        $locations = get_nav_menu_locations();
        if (isset($locations[$menu_name]))
        {
            $result = wp_get_nav_menu_items(
                wp_get_nav_menu_object($locations[$menu_name])->term_id
            );
        }

        return $result;
    }
}

add_action('init', array('MJWKMenus', 'init'));
