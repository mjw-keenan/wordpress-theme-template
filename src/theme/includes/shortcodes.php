<?php

class MJWKShortcodes
{

    public static function init()
    {
        $shortcodes = new MJWKShortcodes();
        $shortcodes->shortcodes();
    }

    public function shortcodes()
    {
        // register the shortcodes for the template
        // add_shortcode(...);

    }

    private function _shortcode_content($content)
    {
        if (! empty($content))
        {
            $content = do_shortcode($content);
        }

        return $content;
    }

    private function _template_shortcode($template, $context = array())
    {
        // render a template, passing in the context variables, and return the
        // result as a string
        $result = "";

        ob_start();

        MJWKTemplates::render_template($template, $context);

        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }

    /*
    public function wrapper_example($attrs, $content)
    {
        if (! empty($content))
        {
            return $this->_template_shortcode(
                '<template-name>',
                array(
                    // ensure we process any shortcodes contained within the
                    // content.
                    "content" => $this->_shortcode_content($content)
                )
            );
        }
        else
        {
            return "";
        }
    }

    public function attribute_example($attrs)
    {
        // extract the known attributes from those specified
        $attrs = shortcode_atts(
            array(
                "arg-1" => "default",
            ),
            $attrs
        );

        return $this->_template_shortcode(
            'templates/link-block.php',
            $attrs
        );
    }
    */
}

add_action('init', array('MJWKShortcodes', 'init'));
