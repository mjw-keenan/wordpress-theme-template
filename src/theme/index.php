<?php
get_template_part('_header');
?>

<div class="container">
  <h1><?php the_title(); ?></h1>
  <?php get_template_part('_content_body'); ?>
</div>

<?php get_template_part('_footer'); ?>
