(function(window, jQuery) {
  jQuery(window).load(function() {
    jQuery("img.lazy").lazyload({
      failure_limit: 10 // set a small limit to look for images below the fold.
                        // this is due to the grid layout on the home page
                        // causing images to be out of sequence
    });
  });
})(window, jQuery);

