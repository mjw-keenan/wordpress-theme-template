<?php

class MJWKFixedHeader
{

    public static $fixed_header_selector = "header";

    public static function init()
    {
        $self = new MJWKFixedHeader();
        $self->_init();
    }

    public function _init()
    {
        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));
    }

    public function wp_enqueue_scripts()
    {
        $script = new MJWKScripts('mjwk-fixed-header',
            MJWKTemplates::$theme_uri . '/lib/blocks/fixed-header/js/fixed-header.js',
            array(MJWKDependencies::$jquery_throttle_debounce->name)
        );
        $script->add_data(array(
            'fixedHeader' => self::$fixed_header_selector
        ));
        $script->enqueue();
    }
}

add_action('init', array('MJWKFixedHeader', 'init'));
