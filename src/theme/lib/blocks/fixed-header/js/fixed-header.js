(function(window, document, jQuery, MJWK) {
  var $window = jQuery(window);

  $window.load(function() {
    var header = jQuery(MJWK.fixedHeader);
    var wpAdminBar = jQuery('#wpadminbar');
    var $body = jQuery('body');

    function fixedHeader() {
      var headerStyles = {};
      var bodyStyles = {};
      // the WP Admin Bar is not fixed under 600px, so we perform the same
      // logic
      var fixedHeaderLimit = 600;

      var height = header.outerHeight(true);

      if ($window.width() > fixedHeaderLimit) {
        // when the wordpress admin bar is present, we need to alter our
        // positioning to account for it.
        if (wpAdminBar.length) {
          headerStyles['top'] = wpAdminBar.outerHeight() + 'px';
        } else {
          headerStyles['top'] = '0px';
        }

        bodyStyles['paddingTop'] = height + 'px';
      } else {
        bodyStyles['paddingTop'] = '';
        headerStyles['top'] = '';
      }

      // apply our calculated styles
      header.css(headerStyles);
      $body.css(bodyStyles);
    }

    // call the fixed header on load
    fixedHeader();

    // also, trigger on window resize
    $window.resize(jQuery.debounce(100, fixedHeader));
  });
})(window, document, jQuery, MJWK);

