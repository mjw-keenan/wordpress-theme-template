<?php

class MJWKDependencies
{
    // A class which registers some common JS dependencies, for other blocks to
    // use

    public static $jquery_throttle_debounce = null;

    public static function wp_enqueue_scripts()
    {
        self::$jquery_throttle_debounce = new MJWKScripts('jquery-throttle-debounce',
            MJWKTemplates::$theme_uri . '/vendor/jquery-throttle-debounce/jquery.ba-throttle-debounce.min.js',
            array('jquery')
        );

        // we only register the script, we don't enqueue it. Wordpress will
        // automatically enqueue it if it is used referenced.
    }
}

add_action(
    'wp_enqueue_scripts',
    array('MJWKDependencies', 'wp_enqueue_scripts'),
    5 // higher priority than standard, to ensure scripts are available before
      // other action hooks run.
);
