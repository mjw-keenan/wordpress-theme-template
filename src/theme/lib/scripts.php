<?php

class MJWKScripts
{
    // A class that others can use to load multiple data pieces, and have them
    // 'localised' at once, in a single object.

    private static $first_footer_script = null;

    private static $data = array();

    private $name = "";

    public function __construct($script_name = "", $uri = "", $deps = array())
    {
        if ($script_name and $uri)
        {
            $deps = ($deps) ? $deps : array();
            $this->name = $script_name;
            self::register_script($script_name, $uri, $deps);
        }
    }

    public function __get($name)
    {
        // grant read-only public access to the script "name" property
        return ($name === "name") ? $this->name : null;
    }

    public function enqueue()
    {
        self::enqueue_script($this->name);
    }

    public function add_data($data)
    {
        self::register_data($data);
    }

    public static function init()
    {
        $self = new MJWKScripts();
        $self->_init();
    }

    public function _init()
    {
        // our code sits in the wp_footer action, at priority 5, which is above
        // the priority that the enqueue_scripts runs, which is 20.
        add_action('wp_footer', array($this, 'wp_footer'), 5);
    }

    // data is only loaded in the footer, loading data into header js is not
    // supported (and generally bad practise).
    public function wp_footer()
    {
        $this->_load_data();
    }

    public static function register_script($name, $uri, $deps = array())
    {
        // a convenience script to register javascript. We always place in the
        // footer, and have no version
        wp_register_script($name, $uri, $deps, null, true);
    }

    public static function enqueue_script($name)
    {
        // enqueue a previously registered script. For the purposes of loading
        // our custom data, we record the first script to be queued, so that
        // we can inject our data at the top of all scripts
        if (self::$first_footer_script === null)
        {
            self::$first_footer_script = $name;
        }

        wp_enqueue_script($name);
    }

    public static function register_data($data)
    {
        // we use array addition (+) so that if there are duplicate keys, the
        // first key retains it's value (the secondary call is ignored).
        self::$data = self::$data + $data;
    }

    private function _load_data()
    {
        if (self::$first_footer_script)
        {
            if (self::$data)
            {
                wp_localize_script(
                    self::$first_footer_script,
                    "MJWK", // register the whole thing under a 'mjwk' object
                    self::$data
                );
            }
        }
    }
}

add_action('init', array('MJWKScripts', 'init'));
