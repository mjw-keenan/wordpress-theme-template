<?php

class MJWKTemplates
{

    public static $theme_uri;
    public static $theme_path;

    public static function render_template($template, $context = array())
    {
        include(locate_template($template));
    }
}

// calculate the theme directory uri once, and we can use it throughout.
MJWKTemplates::$theme_uri = get_stylesheet_directory_uri();
MJWKTemplates::$theme_path = get_template_directory();
